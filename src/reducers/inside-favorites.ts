import { INSIDE_FAVORITES } from '../actions';

const initialState = false;

interface InsideFavorites {
	type: typeof INSIDE_FAVORITES;
	value: boolean;
}

export const insideFavorites = (state = initialState, action: InsideFavorites) => {
	switch (action.type) {
		case INSIDE_FAVORITES:
			return action.value;
		default:
			return state;
	}
};
