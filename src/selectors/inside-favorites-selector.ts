import { State } from '../store';

export const insideFavoritesSelector = (state: State) => state.insideFavorites;
