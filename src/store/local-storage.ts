import { State } from './types';

export const loadState = (): State | undefined => {
	try {
		const serializedState = localStorage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}
		const state = JSON.parse(serializedState);
		return {...state, welcomeModal: {...state.welcomeModal, closed: false}};
	} catch (_) {
		return undefined;
	}
};

export const saveState = (state: State): void => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem('state', serializedState);
	} catch (_) {
		// Error happen.
	}
};
