export { addFavorite, removeFavorite, insideFavorites } from './favorites-dict';
export { shrinkHeader } from './shrink-header';
export { changeView } from './change-view';
export { saveScrollSingle, saveScrollMultiple } from './save-scroll';
export { welcomeModalClosed, welcomeModalHidden } from './welcome-modal';
export { artsCount } from './arts-count';
export * from './types';
