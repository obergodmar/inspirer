import { WELCOME_MODAL_CLOSED, WELCOME_MODAL_HIDDEN } from '../actions';
import { WelcomeModal } from '../store';

const initialState: WelcomeModal = {
	closed: false,
	hidden: false
};

interface WelcomeModalAction {
	type: typeof WELCOME_MODAL_CLOSED | typeof WELCOME_MODAL_HIDDEN;
	hidden: boolean;
	closed: boolean;
}

export const welcomeModal = (state = initialState, action: WelcomeModalAction) => {
	switch (action.type) {
		case WELCOME_MODAL_CLOSED:
			return {...state, closed: action.closed};
		case WELCOME_MODAL_HIDDEN:
			return {...state, hidden: action.hidden};
		default:
			return state;
	}
};
