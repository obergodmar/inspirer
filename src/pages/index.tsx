import * as React from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { graphql } from 'gatsby';

import ru from '../locales/ru.json';
import { artsCount, insideFavorites } from '../actions';
import { Layout } from '../components';
import { Art } from '../store';

interface Props {
	data: {
		allArtsJson: {
			edges: [
				{
					node: Art;
				}
			]
		}
	};
}

const IndexPage = ({data}: Props) => {
	const dispatch = useDispatch();
	const arts = data.allArtsJson.edges.map(art => art.node);

	useEffect(() => {
		document.title = ru['header.main'];
		dispatch(insideFavorites(false));
	}, []);

	useEffect(() => {
		dispatch(artsCount(arts.length));
	}, [arts]);

	return (
		<Layout arts={arts} />
	);
};

export const query = graphql`
  query indexPage {
    allArtsJson {
      edges {
        node {
          dbfId
          file
          image {
            childImageSharp {
                fluid(maxWidth: 1920, srcSetBreakpoints: [ 320, 700, 1280, 1920 ] ) {
	                src
				    srcSet
				    sizes
                    ...GatsbyImageSharpFluid
                }
            }
          }
        }
      }
    }
  }
`;

IndexPage.displayName = 'IndexPage';

export default IndexPage;
