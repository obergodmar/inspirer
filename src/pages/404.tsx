import * as React from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import ru from '../locales/ru.json';
import { insideFavorites } from '../actions';
import { Layout } from '../components';

const UndefinedPage = () => {
	const dispatch = useDispatch();
	useEffect(() => {
		document.title = ru['page.undefined'];
		dispatch(insideFavorites(false));
	}, []);

	return (
		<Layout arts={[]} />
	);
};

UndefinedPage.displayName = 'IndexPage';

export default UndefinedPage;
