import { ARTS_COUNT } from './types';

export const artsCount = (count: number) => ({
	type: ARTS_COUNT,
	count
});
