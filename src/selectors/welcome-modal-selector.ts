import { State } from '../store';

export const welcomeModalSelector = (state: State) => state.welcomeModal;
