import { SHRINK_HEADER } from '../actions';

const initialState = false;

interface ShrinkHeader {
	type: typeof SHRINK_HEADER;
	shrink: boolean;
}

export const shrink = (state = initialState, action: ShrinkHeader) => {
	switch (action.type) {
		case SHRINK_HEADER:
			return action.shrink;
		default:
			return state;
	}
};
