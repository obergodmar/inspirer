import { CHANGE_VIEW } from '../actions';
import { View } from '../store';

const initialState = 'single';

interface ChangView {
	type: typeof CHANGE_VIEW;
	view: View;
}

export const view = (state = initialState, action: ChangView) => {
	switch (action.type) {
		case CHANGE_VIEW:
			return action.view;
		default:
			return state;
	}
};
