import * as React from 'react';
import { forwardRef, useImperativeHandle, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { VariableSizeGrid as Grid } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

import { createItemData } from '../utils';
import { saveScrollSingle } from '../actions';
import { Cell } from './cell/cell';
import { insideFavoritesSelector, scrollSelector } from '../selectors';
import { Art } from '../store';
import { GAP } from '../constants';

interface Props {
	arts: Art[];
	handleClick: (index: number) => void;
	isSmall: boolean;
}

interface RefHandles {
	outerListRef: HTMLDivElement | null;
	innerListRef: HTMLDivElement | null;
}

const innerElementType = forwardRef<any, any>(({ style, ...rest }, ref) => (
	<div
		ref={ref}
		style={{
			...style,
			paddingTop: GAP
		}}
		{...rest}
	/>
));

export const SingleView = forwardRef<RefHandles, Props>(({arts, isSmall, handleClick}: Props, ref) => {
	const artWidth = 700;
	const outerRef = useRef<HTMLDivElement>(null);
	const innerRef = useRef<HTMLDivElement>(null);
	const insideFavorites = useSelector(insideFavoritesSelector);
	const {scrollSingle} = useSelector(scrollSelector);

	useImperativeHandle(ref, () => ({
		get outerListRef() {
			return outerRef.current;
		},
		get innerListRef() {
			return innerRef.current;
		}
	}));

	const dispatch = useDispatch();

	const calculateHeight = (index: number, width: number) => {
		if (!arts[index] || !arts[index].image) {
			return 0;
		}
		return width / arts[index].image.childImageSharp.fluid.aspectRatio + GAP;
	};

	return (
		<AutoSizer>
			{({width, height}) => {
				const columnWidth = width > 731 ? artWidth : width - GAP;
				const itemData = createItemData({
					arts,
					handleClick,
					columnCount: 0,
					isSmall
				});
				return (
					<Grid
						outerRef={outerRef}
						innerRef={innerRef}
						innerElementType={innerElementType}
						width={width}
						height={height}
						columnCount={1}
						columnWidth={() => columnWidth}
						rowCount={arts.length}
						rowHeight={index => calculateHeight(index, columnWidth)}
						itemData={itemData}
						initialScrollTop={!insideFavorites ? scrollSingle : 0}
						onItemsRendered={({
							visibleRowStartIndex
						}) => {
							if (insideFavorites) {
								return;
							}
							const scroll = [...Array(visibleRowStartIndex).keys()]
								.reduce((heightSum, next) => {
									heightSum += calculateHeight(next, columnWidth);
									return heightSum;
								}, 0);
							dispatch(saveScrollSingle(scroll));
						}}
					>
						{Cell}
					</Grid>

				);
			}}
		</AutoSizer>
	);
});

SingleView.displayName = 'SingleView';
