import * as React from 'react';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@chakra-ui/core';

import { scrollSelector, shrinkSelector, viewSelector } from '../selectors';
import { saveScrollMultiple, saveScrollSingle } from '../actions';
import { Art } from '../store';
import { DURATION } from '../constants';
import { SingleView, MultipleView } from '.';

interface Props {
	arts: Art[];
	innerWidth: number;
	keyCode: keyof Keys;
	resetKeyCode: () => void;
	isSmall: boolean;
	handleClick: (index: number) => void;
}

export interface Keys {
	end: number;
	home: number;
	pageUp: number;
	pageDown: number;
}

interface RefHandles {
	outerListRef: HTMLDivElement | null;
	innerListRef: HTMLDivElement | null;
}

export const Arts = ({arts, innerWidth, keyCode, resetKeyCode, isSmall, handleClick}: Props) => {
	const [isMounted, setMounted] = useState(false);
	const view = useSelector(viewSelector);
	const shrink = useSelector(shrinkSelector);
	const {scrollSingle, scrollMultiple} = useSelector(scrollSelector);
	const pageOffset = 1000;
	const scrollOffset = useMemo(() => {
		if (view === 'single') {
			return scrollSingle;
		}
		return scrollMultiple;
	}, [view, scrollSingle, scrollMultiple]);
	const saveScrollDispatch = useMemo(() =>
			view === 'single' ? saveScrollSingle : saveScrollMultiple,
		[view]);

	const listRef = useRef<RefHandles>(null);

	const dispatch = useDispatch();

	useEffect(() => {
		setMounted(true);
	}, []);

	useEffect(() => {
		setMounted(false);
		const timeout = setTimeout(() => {
			setMounted(true);
		}, DURATION);
		return () => {
			clearTimeout(timeout);
		};
	}, [innerWidth]);

	useEffect(() => {
		if (!keyCode || !listRef || !listRef.current) {
			return;
		}
		const {innerListRef, outerListRef} = listRef.current;
		if (!innerListRef || !outerListRef) {
			return;
		}
		const minHeight = 0.1;
		const maxHeight = Number(innerListRef.style.height.replace('px', ''));
		const keys: Keys = {
			pageUp: Math.max(minHeight, scrollOffset - pageOffset),
			pageDown: Math.min(scrollOffset + pageOffset, maxHeight),
			end: maxHeight,
			home: minHeight
		};

		outerListRef.scrollTo({top: keys[keyCode], behavior: 'smooth'});
		if (keyCode === 'pageUp' || keyCode === 'pageDown') {
			dispatch(saveScrollDispatch(keys[keyCode]));
		}

		resetKeyCode();
	}, [keyCode, listRef, scrollOffset]);

	return (
		<>
			{isMounted ? (
				<Box
					zIndex={1}
					transition='height .3s'
					h={shrink ? '94vh' : '90vh'}
				>
					{view === 'single' ? (
						<SingleView
							ref={listRef}
							arts={arts}
							handleClick={handleClick}
							isSmall={isSmall}
						/>
					) : (
						<MultipleView
							ref={listRef}
							arts={arts}
							handleClick={handleClick}
							isSmall={isSmall}
						/>
					)}
				</Box>
			) : null}
		</>
	);
};

Arts.displayName = 'Arts';
