import { WELCOME_MODAL_CLOSED, WELCOME_MODAL_HIDDEN } from './types';

export const welcomeModalClosed = (closed: boolean) => ({
	type: WELCOME_MODAL_CLOSED,
	closed
});

export const welcomeModalHidden = (hidden: boolean) => ({
	type: WELCOME_MODAL_HIDDEN,
	hidden
});
