import { State } from '../store';

export const shrinkSelector = (state: State) => state.shrink;
