import { State } from '../store';

export const scrollSelector = (state: State) => state.scroll;
