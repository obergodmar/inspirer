import * as React from 'react';
import { memo, useCallback, useMemo, useState } from 'react';
import { areEqual } from 'react-window';
import GatsbyImage from 'gatsby-image';
import { Box, IconButton } from '@chakra-ui/core';

import { Art } from '../../store';
import { useDispatch, useSelector } from 'react-redux';
import { favoritesSelector } from '../../selectors';
import { addFavorite, removeFavorite } from '../../actions';
import { GAP } from '../../constants';
import './cell.css';

interface CellProps {
	columnIndex: number;
	rowIndex: number;
	style: any;
	data: {
		arts: Art[];
		columnCount: number;
		isSmall: boolean;
		handleClick: (index: number) => void;
	};
}

export const Cell = memo(({columnIndex, rowIndex, style, data}: CellProps) => {
	const {arts, columnCount, handleClick, isSmall} = data;
	const favorites = useSelector(favoritesSelector);
	const [isHover, setHover] = useState(false);

	const imageIndex = useMemo(() => {
		if (columnCount) {
			return columnIndex + rowIndex * columnCount;
		} else {
			return rowIndex;
		}
	}, [columnIndex, rowIndex, columnCount]);

	const art = useMemo(() => arts[imageIndex], [imageIndex, arts]);

	const isExists = useMemo(() =>
		Boolean(favorites.find(favorite => {
			if (art && art.image) {
				return favorite.dbfId === art.dbfId;
			}
			return false;
		})), [favorites, art]);

	const dispatch = useDispatch();

	const itemClick = useCallback(() => handleClick(imageIndex), [handleClick, imageIndex]);
	const handleHover = useCallback(() => setHover(true), []);
	const handleLeave = useCallback(() => setHover(false), []);

	const handleButtonClick = useCallback((e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		if (isExists) {
			dispatch(removeFavorite(art));
		} else {
			dispatch(addFavorite(art));
		}
	}, [dispatch, isExists, art]);

	return (
		<div style={{
			...style,
			transform: columnCount ? 'unset' : 'translate(-50%)',
			left: columnCount ? style.left + GAP : '50%',
			top: style.top + GAP,
			width: columnCount ? style.width - GAP : style.width - GAP,
			height: style.height - GAP
		}}>
			{art && art.image && (
				<Box
					onMouseEnter={handleHover}
					onMouseLeave={handleLeave}
					position='relative'
					className='cell'
					key={art.dbfId}
					rounded='md'
					shadow='lg'
					onClick={itemClick}
				>
					{isHover && !isSmall && (
						<IconButton
							onClick={handleButtonClick}
							zIndex={4}
							position='absolute'
							right='1rem'
							top='1rem'
							aria-label='favorites'
							icon={isExists ? 'minus' : 'add'}
							size='sm'
						/>
					)}
					<GatsbyImage
						loading='eager'
						fluid={art.image.childImageSharp.fluid}
					/>
				</Box>
			)}
		</div>
	);
}, areEqual);


Cell.displayName = 'Cell';
