import { State } from '../store';

export const artsCountSelector = (state: State) => state.artsCount;
