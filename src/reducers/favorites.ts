import { Favorite } from '../store';
import { ADD_FAVORITE, REMOVE_FAVORITE } from '../actions';

const initialState: Favorite[] = [];

interface UpdateFavorites {
	type: typeof ADD_FAVORITE | typeof REMOVE_FAVORITE;
	favorite: Favorite;
}

export const favorites = (state = initialState, action: UpdateFavorites) => {
	switch (action.type) {
		case ADD_FAVORITE:
			return [...state, action.favorite];
		case REMOVE_FAVORITE:
			return state.filter(art => art.dbfId !== action.favorite.dbfId);
		default:
			return state;
	}
};
