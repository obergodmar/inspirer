import * as React from 'react';
import { Provider } from 'react-redux';

import { createStore, saveState } from './src/store';

const WrapWithProvider = ({element}) => {
	const store = createStore();
	store.subscribe(() => {
		saveState(store.getState());
	});
	return (
		<Provider store={store}>
			{element}
		</Provider>
	);
};

export default WrapWithProvider;
