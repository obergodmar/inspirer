export interface State {
	favorites: Favorite[];
	shrink: boolean;
	insideFavorites: boolean;
	scroll: Scroll;
	view: View;
	artsCount: number;
	welcomeModal: WelcomeModal;
}

export interface WelcomeModal {
	closed: boolean;
	hidden: boolean;
}

export interface Scroll {
	scrollSingle: number;
	scrollMultiple: number;
}

export type View = 'single' | 'multiple';

export type Favorite = Art;

export interface Art {
	dbfId: string;
	name: string;
	image: Image;
}

export interface Image {
	childImageSharp: {
		fluid: {
			aspectRatio: number;
			base64: string;
			sizes: string;
			src: string;
			srcSet: string;
		}
	};
}
