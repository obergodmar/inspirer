import { SAVE_SCROLL_MULTIPLE, SAVE_SCROLL_SINGLE } from './types';

export const saveScrollSingle = (scroll: number) => ({
	type: SAVE_SCROLL_SINGLE,
	scroll
});

export const saveScrollMultiple = (scroll: number) => ({
	type: SAVE_SCROLL_MULTIPLE,
	scroll
});
