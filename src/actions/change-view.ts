import { CHANGE_VIEW } from './types';
import { View } from '../store';

export const changeView = (view: View) => ({
	type: CHANGE_VIEW,
	view
});
