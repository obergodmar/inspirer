import * as React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ru from '../locales/ru.json';
import { insideFavorites } from '../actions';
import { favoritesSelector } from '../selectors';
import { Layout } from '../components';

const FavoritesPage = () => {
	const favorites = useSelector(favoritesSelector);
	const dispatch = useDispatch();

	useEffect(() => {
		document.title = ru['title.favorites'];
		dispatch(insideFavorites(true));
	}, []);

	return (
		<Layout arts={favorites} />
	);

};

FavoritesPage.dispayName = 'FavoritesPage';

export default FavoritesPage;
