import { State } from '../store';

export const viewSelector = (state: State) => state.view;
