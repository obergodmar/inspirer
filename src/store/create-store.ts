import { createStore as ReduxCreateStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';

import { loadState } from './local-storage';
import reducer from '../reducers';

const persistedState = loadState();
const createStore = () => ReduxCreateStore(reducer, persistedState, devToolsEnhancer({}));

export default createStore;
