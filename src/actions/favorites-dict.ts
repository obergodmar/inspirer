import { Favorite } from '../store';
import { ADD_FAVORITE, INSIDE_FAVORITES, REMOVE_FAVORITE } from './types';

export const addFavorite = (favorite: Favorite) => ({
	type: ADD_FAVORITE,
	favorite
});

export const removeFavorite = (favorite: Favorite) => ({
	type: REMOVE_FAVORITE,
	favorite
});

export const insideFavorites = (value: boolean) => ({
	type: INSIDE_FAVORITES,
	value
});
