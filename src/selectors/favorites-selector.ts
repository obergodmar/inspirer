import { State } from '../store';

export const favoritesSelector = (state: State) => state.favorites;
