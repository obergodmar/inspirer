import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ColorModeProvider, CSSReset, Flex, theme } from '@chakra-ui/core';
import { ThemeProvider } from 'emotion-theming';
import debounce from 'lodash.debounce';
import { navigate } from 'gatsby';

import { shrinkHeader } from '../actions';
import { Header } from './header';
import { Arts, Keys } from './arts';
import { Viewer } from './viewer/viewer';
import { Art } from '../store';
import { WelcomeModal } from './welcome-modal';
import { insideFavoritesSelector } from '../selectors';
import { MAIN } from '../constants';

interface Props {
	arts: Art[];
}

const customTheme = {
	...theme,
	fonts: {
		...theme.fonts,
		heading: '"Pangolin", cursive',
		body: '"Roboto", sans-serif'
	}
};

export const Layout = ({arts}: Props) => {
	const [keyCode, setKeyCode] = useState<string | undefined>(undefined);
	const [isSmall, setSmall] = useState(false);
	const [isViewerShown, setViewerShown] = useState(false);
	const [viewedImageIndex, setViewedImageIndex] = useState(0);
	const insideFavorites = useSelector(insideFavoritesSelector);
	const [width, setWidth] = useState(0);

	const dispatch = useDispatch();

	const handleArtClick = useCallback((artIndex: number) => {
		setViewedImageIndex(artIndex);
		setViewerShown(true);
	}, []);

	const handleClose = useCallback(() => setViewerShown(false), []);

	const resetKeyCode = useCallback(() => setKeyCode(undefined), []);

	useEffect(() => {
		const handleKeyDown = (e: KeyboardEvent) => {
			const {code} = e;
			switch (code) {
				case 'Escape':
					if (insideFavorites) {
						return navigate(MAIN);
					}
					break;
				case 'Home':
				case 'End':
				case 'PageUp':
				case 'PageDown':
					setKeyCode(code.charAt(0).toLowerCase() + code.slice(1));
					break;
				case 'ArrowUp':
					setKeyCode('pageUp');
					break;
				case 'ArrowDown':
					setKeyCode('pageDown');
					break;
			}
		};
		window.addEventListener('keydown', handleKeyDown);
		return () => {
			window.removeEventListener('keydown', handleKeyDown);
		};
	}, [insideFavorites]);

	useEffect(() => {
		document.body.style.overflow = 'hidden';
		const handleResize = debounce(() => {
			const {innerWidth} = window;
			setWidth(innerWidth);
			if (innerWidth > 731) {
				setSmall(false);
			} else {
				setSmall(true);
				dispatch(shrinkHeader(true));
			}
		}, 50);

		handleResize();
		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);

	return (
		<ThemeProvider theme={customTheme}>
			<ColorModeProvider>
				<CSSReset />
				<Flex direction='column'>
					<Header
						isSmall={isSmall}
					/>
					<Arts
						keyCode={keyCode as keyof Keys}
						resetKeyCode={resetKeyCode}
						innerWidth={width}
						isSmall={isSmall}
						arts={arts}
						handleClick={handleArtClick}
					/>
					{isViewerShown && (
						<Viewer
							arts={arts}
							artIndex={viewedImageIndex}
							handleClose={handleClose}
						/>
					)}
				</Flex>
				<WelcomeModal />
			</ColorModeProvider>
		</ThemeProvider>
	);
};

Layout.displayName = 'Layout';
