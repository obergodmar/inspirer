const {app, Menu, shell, BrowserWindow} = require('electron');
const net = require('net');
const http = require('http');
const path = require('path');
const nodeStatic = require('node-static');

const server = (initialPort) => {
    return new Promise((resolve) => {
        const getFreePort = (port) => {
            const dir = path.join(__dirname, '..', 'public');
            const file = new nodeStatic.Server(dir);
            const connection = net.createConnection({port});
            connection.on('connect', () => {
                connection.destroy();
                getFreePort(port + 1);
            });
            connection.on('error', () => {
                http
                    .createServer((req, res) => {
                        req
                            .addListener('end', function () {
                                file.serve(req, res);
                            })
                            .resume();
                    })
                    .listen(port, () => {
                        resolve({port, connection});
                    });
            });
        };
        getFreePort(initialPort);
    });
};

const template = () => [
    {
        label: app.name,
        submenu: [{role: 'quit'}],
    },
    {
        label: 'Вид',
        submenu: [
            {role: 'reload'},
            {role: 'forcereload'},
            {role: 'toggledevtools'},
            {type: 'separator'},
            {role: 'resetzoom'},
            {role: 'zoomin'},
            {role: 'zoomout'},
            {type: 'separator'},
            {role: 'togglefullscreen'},
        ],
    },
    {
        label: 'Окно',
        submenu: [
            {role: 'minimize'},
            {role: 'zoom'},
        ],
    },
    {
        role: 'О приложении',
        submenu: [
            {
                label: 'Создатель',
                click: async () => {
                    await shell.openExternal('https://gitlab.com/obergodmar');
                },
            },
        ],
    }
];

const menu = () => Menu.buildFromTemplate(template());

const mainWindow = (port) => {
    const window = new BrowserWindow({
        height: 720,
        width: 1280,
        autoHideMenuBar: true
    });
    window.loadURL(`http://localhost:${port}`)
        .then(status => console.log(`Window: ${status}`));
    const appMenu = menu();
    window.setMenu(appMenu);
    return window;
};

const START_LOOKING_FOR_PORT_FROM = 30000;

let holdMainWindowOnMemory;

const init = async () => {
    const {port, connection} = await server(START_LOOKING_FOR_PORT_FROM);
    holdMainWindowOnMemory = mainWindow(port);
    app.on('window-all-closed', () => {
        connection.destroy();
        app.quit();
    });
};

app.on('ready', () => {
    init().then(status => console.log(`Started: ${status}`));
});
