import memoize from 'memoize-one';

export const createItemData = memoize(({arts, handleClick, columnCount, isSmall}) => ({
	arts,
	handleClick,
	columnCount,
	isSmall
}));
