import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { navigate } from 'gatsby';
import { Button, Flex, Heading, IconButton, Text, useColorMode } from '@chakra-ui/core';
import { MdViewArray, MdViewModule } from 'react-icons/md';
import { FaStar } from 'react-icons/fa';
import { AiOutlineShrink } from 'react-icons/ai';

import { insideFavoritesSelector, shrinkSelector, viewSelector } from '../selectors';
import ru from '../locales/ru.json';
import { changeView, shrinkHeader } from '../actions';
import { View } from '../store';
import { FAVORITES, MAIN } from '../constants';

interface Props {
	isSmall: boolean;
}

export const Header = ({isSmall}: Props) => {
	const [isMounted, setMounted] = useState(false);
	const shrink = useSelector(shrinkSelector);
	const view = useSelector(viewSelector);
	const insideFavorites = useSelector(insideFavoritesSelector);
	const {colorMode, toggleColorMode} = useColorMode();

	const dispatch = useDispatch();

	const handleClickChangeTheme = useCallback(() => toggleColorMode(), [toggleColorMode]);

	const handleShrink = useCallback(() => {
			dispatch(shrinkHeader(!shrink));
		},
		[dispatch, shrink]);

	const handleChangeView = useCallback((viewValue: View) =>
			dispatch(changeView(viewValue)),
		[dispatch]);

	const headerClick = useCallback(() => navigate(MAIN), []);

	const favoritesClick = useCallback(() => {
		if (insideFavorites) {
			return navigate(MAIN);
		} else {
			return navigate(FAVORITES);
		}
	}, [insideFavorites]);

	useEffect(() => {
		setMounted(true);
	}, []);

	const viewSingleCallback = useCallback(() =>
			handleChangeView('single'),
		[handleChangeView]);

	const viewMultipleCallback = useCallback(() =>
			handleChangeView('multiple'),
		[handleChangeView]);

	return (
		<>
			{isMounted ? (
				<Flex
					zIndex={2}
					userSelect='none'
					padding={shrink ? '.2rem' : '1rem'}
					h={shrink ? '6vh' : '10vh'}
					transition='padding .3s, height .3s'
					align='center'
					justify='space-between'
					shadow='md'
				>
					<Flex alignItems='flex-end'>
						<Heading
							transition='font-size .3s, margin-left .3s'
							cursor='pointer'
							as='h1'
							size={isSmall ? 'sm' : shrink ? 'md' : 'lg'}
							ml={shrink ? '0.3rem' : ''}
							onClick={headerClick}
						>
							{ru['header.name']}
						</Heading>
						{!isSmall && (
							<Text ml='.4rem' fontSize='.9rem'>
								{`/ ${insideFavorites ? ru['header.favorites'] : ru['header.main']}`}
							</Text>
						)}
					</Flex>
					<Flex
						w={isSmall ? '150px' : shrink ? '320px' : '420px'}
						mr={shrink ? '0.3rem' : ''}
						transition='width .3s, margin-right .3s'
						align='center'
						justify='space-between'
						fontFamily='body'
					>
						<Flex
							align='center'
							justify='center'
						>
							<IconButton
								onClick={viewSingleCallback}
								size={shrink ? 'sm' : 'lg'}
								aria-label='view'
								fontSize='30px'
								icon={MdViewArray}
								variant={view === 'single' ? 'solid' : 'outline'}
							/>
							<IconButton
								onClick={viewMultipleCallback}
								size={shrink ? 'sm' : 'lg'}
								ml={shrink ? '.2rem' : '.5rem'}
								aria-label='view'
								fontSize='30px'
								icon={MdViewModule}
								variant={view === 'multiple' ? 'solid' : 'outline'}
							/>
						</Flex>
						{!isSmall && (
							<IconButton
								size={shrink ? 'sm' : 'lg'}
								onClick={handleShrink}
								aria-label='reduce'
								variant='outline'
								icon={AiOutlineShrink}
							/>
						)}
						<IconButton
							size={shrink ? 'sm' : 'lg'}
							onClick={handleClickChangeTheme}
							aria-label='theme'
							icon={colorMode === 'dark' ? 'moon' : 'sun'}
							variant='outline'
						/>
						{isSmall ? (
							<IconButton
								onClick={favoritesClick}
								size={shrink ? 'sm' : 'lg'}
								aria-label='favorites'
								variant={insideFavorites ? 'solid' : 'outline'}
								icon={FaStar}
							/>
						) : (
							<Button
								size={shrink ? 'sm' : 'lg'}
								onClick={favoritesClick}
								rightIcon={FaStar}
								variant={insideFavorites ? 'solid' : 'outline'}
							>
								{ru['header.button.favorites']}
							</Button>
						)}
					</Flex>
				</Flex>
			) : null}
		</>
	);
};

Header.displayName = 'Header';
