import { ARTS_COUNT } from '../actions';

const initialState = 0;

interface ArtsCount {
	type: typeof ARTS_COUNT;
	count: number;
}

export const artsCount = (state = initialState, action: ArtsCount) => {
	switch (action.type) {
		case ARTS_COUNT:
			return action.count;
		default:
			return state;
	}
};
