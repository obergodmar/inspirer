import { combineReducers } from 'redux';
import { favorites } from './favorites';
import { insideFavorites } from './inside-favorites';
import { shrink } from './shrink';
import { scroll } from './scroll';
import { view } from './view';
import { welcomeModal } from './welcome-modal';
import { artsCount } from './arts-count';

export default combineReducers({
	view,
	scroll,
	shrink,
	favorites,
	welcomeModal,
	artsCount,
	insideFavorites
});
