import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import {
	Checkbox,
	Divider,
	Flex,
	Icon,
	Link,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Text,
	useDisclosure
} from '@chakra-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import { artsCountSelector, insideFavoritesSelector, welcomeModalSelector } from '../selectors';
import ru from '../locales/ru.json';
import { welcomeModalClosed, welcomeModalHidden } from '../actions';

export const WelcomeModal = () => {
	const {isOpen, onOpen, onClose} = useDisclosure();
	const [isChecked, setChecked] = useState(false);
	const {hidden, closed} = useSelector(welcomeModalSelector);
	const insideFavorites = useSelector(insideFavoritesSelector);
	const artsCount = useSelector(artsCountSelector);

	const dispatch = useDispatch();

	useEffect(() => {
		const timeout = setTimeout(() => {
			if (!hidden && !closed && !insideFavorites) {
				onOpen();
			}
		}, 100);
		return () => {
			clearTimeout(timeout);
		};
	}, [hidden, closed, insideFavorites, onOpen]);

	const handleClose = useCallback(() => {
		dispatch(welcomeModalClosed(true));
		if (isChecked) {
			dispatch(welcomeModalHidden(true));
		}
		onClose();
	}, [dispatch, isChecked, onClose]);

	const handleChange = useCallback(() => setChecked(!isChecked), [isChecked]);

	return (
		<Modal isOpen={isOpen} onClose={handleClose}>
			<ModalOverlay />
			<ModalContent rounded='md'>
				<ModalHeader>{ru['modal.header']}</ModalHeader>
				<ModalCloseButton />
				<ModalBody>
					<Text>
						{ru['modal.content.part-1']}
						<Text as='b'> "{ru['header.name']}" </Text>
						{ru['modal.content.part-2']}
					</Text>
					<Text pt='1rem'>
						{`${ru['modal.content.part-3']} `}
						<Text as='b'>{artsCount}</Text>.
					</Text>
					<Text pt='1rem'>
						{`${ru['modal.content.part-4']} `}
						<Link color='blue.500' href='https://obergodmar.gitlab.io/sancho-mucho/' isExternal>
							{ru['modal.content.link']}<Icon name='external-link' mx='.5rem' />
						</Link>
					</Text>
					<Divider />
					<Flex direction='column' alignItems='flex-end'>
						<Text pt='1rem' as='em' textAlign='right' float='right'>
							"{ru['modal.content.part-5']}"
						</Text>
						<Text pt='1rem' textAlign='right' float='right' fontSize='.8rem'>
							{ru['modal.content.date']}
						</Text>
						<Text textAlign='right' float='right' fontSize='.8rem'>
							{ru['modal.content.author']}
						</Text>
					</Flex>
				</ModalBody>
				<ModalFooter display='flex' justifyContent='space-between'>
					<Checkbox
						isChecked={isChecked}
						onChange={handleChange}
					>
						{ru['modal.content.checkbox']}
					</Checkbox>
				</ModalFooter>
			</ModalContent>
		</Modal>
	);
};

WelcomeModal.displayName = 'WelcomeModal';
