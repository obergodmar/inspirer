import * as React from 'react';
import { KeyboardEvent, MouseEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Button, DarkMode, IconButton } from '@chakra-ui/core';
import { AiOutlineLeft, AiOutlineRight } from 'react-icons/ai';
import GatsbyImage from 'gatsby-image';
import { useDispatch, useSelector } from 'react-redux';

import ru from '../../locales/ru.json';
import { favoritesSelector } from '../../selectors';
import { addFavorite, removeFavorite } from '../../actions';
import { DURATION } from '../../constants';
import { Art } from '../../store';
import './viewser.css';

interface Props {
	arts: Art[];
	artIndex: number;
	handleClose: () => void;
}

export const Viewer = ({artIndex, arts, handleClose}: Props) => {
	const viewerRef = useRef<HTMLDivElement>(null);
	const [isShown, setShown] = useState(false);
	const [artAnimated, setArtAnimated] = useState(false);
	const [artLoaded, setArtLoaded] = useState(false);
	const favorites = useSelector(favoritesSelector);

	const [currentIndex, setCurrentIndex] = useState(artIndex);
	const art = useMemo(() => arts[currentIndex], [arts, currentIndex]);
	const [currentArt, setCurrentArt] = useState(art);

	const dispatch = useDispatch();

	const isExists = useMemo(() =>
		Boolean(favorites.find(favorite => {
			if (currentArt && currentArt.image) {
				return favorite.dbfId === currentArt.dbfId;
			}
			return false;
		})), [favorites, currentArt]);

	useEffect(() => {
		if (viewerRef && viewerRef.current) {
			viewerRef.current.focus();
		}
	}, [viewerRef]);

	useEffect(() => {
		const timeout = setTimeout(() => {
			setShown(true);
		}, DURATION);
		return () => {
			clearTimeout(timeout);
		};
	}, []);

	useEffect(() => {
		const timeout = setTimeout(() => {
			setCurrentArt(art);
		}, DURATION);
		return () => {
			clearTimeout(timeout);
		};
	}, [art]);

	useEffect(() => {
		setArtAnimated(false);
		setArtLoaded(false);
		const timeout = setTimeout(() => {
			setArtAnimated(true);
		}, DURATION);

		return () => {
			clearTimeout(timeout);
		};
	}, [currentIndex]);

	const handleLoad = useCallback(() => setArtLoaded(true), []);

	const closeCallback = useCallback(() => {
		setShown(false);
		setTimeout(() => {
			handleClose();
		}, DURATION);
	}, [handleClose]);

	const handleKeyDown = useCallback((e: KeyboardEvent) => {
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		switch (e.keyCode) {
			case 27:
				closeCallback();
				break;
			case 37:
				if (!currentIndex) {
					return;
				}
				setCurrentIndex(currentIndex - 1);
				break;
			case 39:
				if (currentIndex + 1 === arts.length) {
					return;
				}
				setCurrentIndex(currentIndex + 1);
				break;
		}
	}, [closeCallback, currentIndex]);

	const handleButtonClick = useCallback((e: MouseEvent) => {
		if (!currentArt || !currentArt.image) {
			return;
		}
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		if (isExists) {
			dispatch(removeFavorite(currentArt));
		} else {
			dispatch(addFavorite(currentArt));
		}
	}, [dispatch, currentArt, isExists]);

	const handleClick = useCallback(() => closeCallback(), [closeCallback]);

	const handleGoLeft = useCallback((e: MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		setCurrentIndex(currentIndex - 1);
	}, [currentIndex]);

	const handleGoRight = useCallback((e: MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		setCurrentIndex(currentIndex + 1);
	}, [currentIndex]);

	return (
		<div
			ref={viewerRef}
			tabIndex={0}
			onClick={handleClick}
			onKeyDown={handleKeyDown}
			className={`viewer ${isShown ? 'viewer--shown' : ''}`}>
			<div className='viewer-gap'>
				<DarkMode>
					{Boolean(currentIndex) && (
						<IconButton
							onClick={handleGoLeft}
							zIndex={4}
							color='white'
							position='fixed'
							size='lg'
							left='2vw'
							top='50%'
							transform='translateY(-50%)'
							aria-label='left'
							icon={AiOutlineLeft}
						/>
					)}
				</DarkMode>
			</div>
			<div
				style={artAnimated ? artLoaded ? (
					{
						opacity: '1'
					}
				) : (
					{
						opacity: '0'
					}
				) : (
					{
						opacity: '0'
					}
				)}
				className='viewer-image'>
				{currentArt && currentArt.image && (
					<GatsbyImage
						onLoad={handleLoad}
						fluid={currentArt.image.childImageSharp.fluid}
					/>
				)}
			</div>
			<div className='viewer-gap'>
				<DarkMode>
					{currentIndex + 1 !== arts.length && (
						<IconButton
							onClick={handleGoRight}
							zIndex={4}
							color='white'
							position='fixed'
							size='lg'
							right='2vw'
							top='50%'
							transform='translateY(-50%)'
							aria-label='right'
							icon={AiOutlineRight}
						/>
					)}
				</DarkMode>
			</div>
			<DarkMode>
				{artLoaded && (
					<Button
						position='absolute'
						top='2vh'
						left='50%'
						size='lg'
						color='white'
						transform='translateX(-50%)'
						onClick={handleButtonClick}
					>
						{isExists ? ru['button.favorites.remove'] : ru['button.favorites.add']}
					</Button>
				)}
			</DarkMode>
		</div>
	);
};

Viewer.displayName = 'Viewer';
