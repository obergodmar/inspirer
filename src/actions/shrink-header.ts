import { SHRINK_HEADER } from './types';

export const shrinkHeader = (shrink: boolean) => ({
	type: SHRINK_HEADER,
	shrink
});
