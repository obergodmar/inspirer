const fs = require('fs');

fs.readdir('../src/data/arts/images', (error, files) => {
    const output = [];
    files.forEach(file => (
        output.push({
            image: `images/${file}`,
            file,
            dbfId: file.replace('.jpg', '')
        })
    ))
    fs.writeFile('../src/data/arts/arts.json', JSON.stringify(output), (err) => {
        if (err) return console.log(err);
    });
    console.log(`${output.length || 0} files`);
});
