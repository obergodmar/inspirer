module.exports = {
    siteMetadata: {
        title: 'Воодушевлятор',
        description: 'Приложение для просмотра тысяч потрясающих художественных артов',
        author: '@obergodmar'
    },
    pathPrefix: '/inspirer',
    plugins: [
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'arts',
                path: `${__dirname}/src/data/arts`
            }
        },
        'gatsby-transformer-sharp',
        'gatsby-plugin-sharp',
        'gatsby-transformer-json',
        {
            resolve: 'gatsby-plugin-favicon',
            options: {
                logo: './src/favicon.png',
                appName: 'Воодушевлятор',
                appDescription: 'Приложение для просмотра тысяч потрясающих художественных артов',
                developerName: 'obergodmar',
                developerURL: 'https://github.com/obergodmar',
                dir: 'auto',
                lang: 'ru-RU',
                background: '#fff',
                theme_color: '#fff',
                display: 'standalone',
                orientation: 'any',
                start_url: '/',
                version: '1.0',

                icons: {
                    android: true,
                    appleIcon: true,
                    appleStartup: true,
                    coast: true,
                    favicons: true,
                    firefox: true,
                    yandex: true,
                    windows: true
                }
            }
        },
        {
            resolve: "gatsby-plugin-chakra-ui",
            options: {
                isResettingCSS: true,
                isUsingColorMode: true,
            },
        },
        'gatsby-plugin-emotion',
        'gatsby-plugin-no-sourcemaps'
    ]
};
