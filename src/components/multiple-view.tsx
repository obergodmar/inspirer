import * as React from 'react';
import { forwardRef, useImperativeHandle, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { VariableSizeGrid as Grid } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';

import { createItemData } from '../utils';
import { saveScrollMultiple } from '../actions';
import { Cell } from './cell/cell';
import { insideFavoritesSelector, scrollSelector } from '../selectors';
import { Art } from '../store';
import { GAP } from '../constants';

interface Props {
	arts: Art[];
	handleClick: (index: number) => void;
	isSmall: boolean;
}

interface RefHandles {
	outerListRef: HTMLDivElement | null;
	innerListRef: HTMLDivElement | null;
}

const innerElementType = forwardRef<any, any>(({style, ...rest}, ref) => (
	<div
		ref={ref}
		style={{
			...style,
			paddingLeft: GAP,
			paddingTop: GAP
		}}
		{...rest}
	/>
));

export const MultipleView = forwardRef<RefHandles, Props>(({arts, isSmall, handleClick}: Props, ref) => {
	const outerListRef = useRef<HTMLDivElement>(null);
	const innerListRef = useRef<HTMLDivElement>(null);
	const insideFavorites = useSelector(insideFavoritesSelector);
	const {scrollMultiple} = useSelector(scrollSelector);

	useImperativeHandle(ref, () => ({
		get outerListRef() {
			return outerListRef.current;
		},
		get innerListRef() {
			return innerListRef.current;
		}
	}));

	const dispatch = useDispatch();

	const calculateMultipleHeight = (index: number, width: number, columnCount: number) => {
		const currentHeight = [...Array(columnCount).keys()].reduce((maxHeight, next) => {
			const singleColumnIndex = next + index * columnCount;
			if (!arts[singleColumnIndex] || !arts[singleColumnIndex].image) {
				return maxHeight;
			}
			const height = width / arts[singleColumnIndex].image.childImageSharp.fluid.aspectRatio;
			return height > maxHeight ? height : maxHeight;
		}, 0);

		return currentHeight + GAP;
	};

	return (
		<AutoSizer>
			{({width, height}) => {
				const columnWidth = width > 731 ? (width - GAP*7) / 5 : (width - GAP*4) / 3;
				const columnCountCalculate = Math.floor(width / columnWidth);
				const columnCount = columnCountCalculate === 6 ? 5 : columnCountCalculate || 1;
				const rowCount = Math.ceil(arts.length / columnCount);
				const itemData = createItemData({
					arts,
					handleClick,
					columnCount,
					isSmall
				});
				return (
					<Grid
						outerRef={outerListRef}
						innerRef={innerListRef}
						innerElementType={innerElementType}
						width={width}
						height={height}
						columnCount={columnCount}
						columnWidth={() => columnWidth + GAP}
						rowCount={rowCount}
						rowHeight={index => calculateMultipleHeight(index, columnWidth, columnCount)}
						itemData={itemData}
						initialScrollTop={!insideFavorites ? scrollMultiple : 0}
						onItemsRendered={({
							visibleRowStartIndex
						}) => {
							if (insideFavorites) {
								return;
							}
							const scroll = [...Array(visibleRowStartIndex).keys()]
								.reduce((heightSum, next) => {
									heightSum += calculateMultipleHeight(next, columnWidth, columnCount);
									return heightSum;
								}, 0);
							dispatch(saveScrollMultiple(scroll));
						}}
					>
						{Cell}
					</Grid>
				);
			}}
		</AutoSizer>
	);
});

MultipleView.displayName = 'MultipleView';
