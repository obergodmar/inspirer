import { SAVE_SCROLL_MULTIPLE, SAVE_SCROLL_SINGLE } from '../actions';
import { Scroll } from '../store';

const initialState: Scroll = {
	scrollSingle: 0,
	scrollMultiple: 0
};

interface ScrollAction {
	type: typeof SAVE_SCROLL_SINGLE | typeof SAVE_SCROLL_MULTIPLE;
	scroll: number;
}

export const scroll = (state = initialState, action: ScrollAction) => {
	switch (action.type) {
		case SAVE_SCROLL_SINGLE:
			return {
				...state, scrollSingle: action.scroll
			};
		case SAVE_SCROLL_MULTIPLE:
			return {
				...state, scrollMultiple: action.scroll
			};
		default:
			return state;
	}
};
